package com.journey.pageobjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class DeleteProjectPage extends BaseDriverClass {

	private WebElement btn_trois_point;
	private WebElement btn_delete;
	private WebElement btn_confirm_delete;
	public DeleteProjectPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public WebElement scroll_find_element(String projetName) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		//WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		WebElement elementToFind = null;


			List<WebElement> titleElements = wait
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("h5")));
			for (WebElement element : titleElements) {
				if (element.getText().equals(projetName)) {
					elementToFind = element;
					break;
				}
			}

			if (elementToFind == null) {
				throw new NoSuchElementException("Cannot find project with title " + projetName);
			}

			js.executeScript("arguments[0].scrollIntoView(true);", elementToFind);

							

		
		return elementToFind;
	}

	public WebElement clickDeleteMenu(WebElement projectToDelete) throws InterruptedException {
		
		Thread.sleep(3000);
		btn_trois_point = projectToDelete.findElement(By.xpath("parent::*")).findElement(By.xpath("following-sibling::*")).findElement(By.className("ant-dropdown-trigger"));
		waitUntilElementVisible(btn_trois_point);
		btn_trois_point.click();
		Thread.sleep(3000);
		//find the delete option menu : After click on 3 points button a menu will be displayed then I will be able to select the delete option menu 
		btn_delete = driver.findElement(By.className("cdk-overlay-container")).findElement(By.className("ant-dropdown-menu-vertical")).findElement(By.xpath("./li[4]"));
		waitUntilElementVisible(btn_delete);
		btn_delete.click();
		return null;
	}
	public void confirm_delete() {
		btn_confirm_delete = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"cdk-overlay-1\"]/nz-modal-container/div/div/div[3]/button[2]")));
		btn_confirm_delete.click();
		 driver.navigate().refresh();
	}
	
	
}
