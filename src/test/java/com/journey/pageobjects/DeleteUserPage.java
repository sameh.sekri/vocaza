package com.journey.pageobjects;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class DeleteUserPage extends BaseDriverClass {

	public DeleteUserPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);

	}

	public WebElement find_user(String username) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ant-table-content")));

		List<WebElement> rows = driver.findElement(By.className("ant-table-content")).findElement(By.xpath("//tbody"))
				.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			// Récupérer la troisième colonne
			WebElement email = row.findElements(By.tagName("td")).get(2);

			// Récupérer le texte de la troisième colonne
			String emailText = email.getText();
			if (username.equals(emailText)) {

				System.out.println("L'utilisateur " + emailText + " est créé.");
				return row;
			}
		}

		return null;

	}

	public void click_delete(WebElement user) {
		Assert.assertTrue("L'utilisateur n'est pas trouvé", user != null);
		user.click();
		WebElement btn_delete_user = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//car-user-detail/div/nz-card/div/div[2]/button")));
		btn_delete_user.click();

	}

	public void confirm_delete(String username) {
		WebElement btn_delete = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath("//nz-modal-container/div/div/div[3]/button[2]")));
		btn_delete.click();
		WebElement user=find_user(username);
		Assert.assertTrue("L'utilisateur non supprimé", user == null);

	}

}
