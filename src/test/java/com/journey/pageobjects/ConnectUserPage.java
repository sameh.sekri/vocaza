package com.journey.pageobjects;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.journey.common.BaseDriverClass;
import com.journey.common.GmailOAuth2Utils;

public class ConnectUserPage extends BaseDriverClass {

	public ConnectUserPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);

	}

	public void first_connect() {

		WebElement firstConnect = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//a[contains(text(),'Première connexion / Mot de passe oublié ?')]")));

		firstConnect.click();
	}

	public void enter_email(String email) {
		WebElement enterEmail = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"username\"]")));
		enterEmail.sendKeys(email);
		WebElement btnValider = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='submit']")));
		btnValider.click();

	}

	public void verify_email(String username) throws Exception {
		GmailOAuth2Utils gmailOAuth2Utils = new GmailOAuth2Utils();

		// Connect to email using Oauth2
		Gmail service = gmailOAuth2Utils.connectUser();

		// Get List Unread messages
		String query = "from:journey@vocaza.net subject:Réinitialisation de mot de passe Vocaza Journey is:unread";

		List<Message> messages = gmailOAuth2Utils.getMessages(service, username, query);
		Assert.assertTrue("Aucun email n'est trouvé non lu venant de journey@vocaza.net.",
				messages != null && !messages.isEmpty());

		// Get the first email unread and extract URL of change password action
		String emailContent = gmailOAuth2Utils.getEmailContent(messages.get(0));
		List<String> urls = gmailOAuth2Utils.extractUrls(emailContent);

		String changePasswordUrl = null;

		for (String url : urls) {
			// extract email links
			System.out.println(url);

			if (url.contains("login-actions")) {
				changePasswordUrl = url;
				break;
			}
		}

		Assert.assertTrue("Aucune URL trouvée contenant l'action login-actions",
				changePasswordUrl != null && !changePasswordUrl.isBlank());

		// open only URL of change password action
		driver.get(changePasswordUrl);
		System.out.println("URL change password opened");
	}

	public void change_password(String newPassword) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='submit']")));

		WebElement passwordTxt = driver.findElement(By.id("password-new"));
		passwordTxt.sendKeys(newPassword);

		WebElement passwordConfirmTxt = driver.findElement(By.id("password-confirm"));
		passwordConfirmTxt.sendKeys(newPassword);

		WebElement btnSend = driver.findElement(By.xpath("//input[@type='submit']"));
		btnSend.click();

		System.out.println("password changed");
	}

	public void verify_project_page(String username,String password) {
		String currentPageUrl = driver.getCurrentUrl();

		if(!currentPageUrl.contains("projects")) {
			
			driver.get("https://qa-journey.vocaza.com/");
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("kc-login")));
			
			WebElement usernameTxt = driver.findElement(By.id("username"));
			usernameTxt.sendKeys(username);

			WebElement passwordTxt = driver.findElement(By.id("password"));
			passwordTxt.sendKeys(password);
			
			WebElement btnSend = driver.findElement(By.id("kc-login"));
			btnSend.click();
			
			//wait 5 sec before check again the current url redirection
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			currentPageUrl = driver.getCurrentUrl();
			
			Assert.assertTrue("Erreur de connexion " + username + " en utilisant ces credentials", currentPageUrl.contains("projects"));
		}
		
		System.out.println("Utilisateur connecté directement après changement du mot de passe.");
	}

}
