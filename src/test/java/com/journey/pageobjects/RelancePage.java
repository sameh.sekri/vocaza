package com.journey.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.python.modules.thread.thread;

import com.journey.common.BaseDriverClass;

public class RelancePage extends BaseDriverClass {

	@FindBy(xpath = "//div[1]/h3/button")
	@CacheLookup
	private WebElement btn_add_relance;
	private WebElement btn_update_sequence;
	private WebElement radio_add_media;
	private WebElement btn_valider;
	private WebElement name;
	private WebElement prefix;
	private WebElement object_email;

	public RelancePage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void add_relance() {
		btn_add_relance = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//car-display-scenario/div[1]/h3/button")));
		btn_add_relance.click();

	}

	public void add_email_relance() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"cdk-overlay-1\"]/nz-modal-container/div")));
		
		radio_add_media =  driver.findElements(By.xpath("//input[@type='radio']")).get(0);
		radio_add_media.click();
		
		btn_valider = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//*[@id=\"cdk-overlay-1\"]/nz-modal-container/div/div/div[3]/button")));
		btn_valider.click();

	}

	public void create_email() {

		name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='name']")));

		name.sendKeys("test");

		prefix = name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='email']")));

		prefix.sendKeys("test1.test");

		object_email = name = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//textarea[@class='ant-input ant-input-stepperless ng-untouched ng-pristine ng-valid']")));

		object_email.sendKeys("Relance");
		WebElement image_logo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[1]/div/car-mail-column/div/app-visual-element")));
		image_logo.click();
		WebElement add_image = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-lg']")));
		add_image.click();
		WebElement image_selected = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//car-imagelibrary/div[2]/div[1]/div/div[2]")));
		image_selected.click();
		WebElement btn_add = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-primary ant-btn-sm']")));
		btn_add.click();

	}

	public void publish_relance() {

		WebElement btn_publish = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//nz-space/div[2]/button")));

		btn_publish.click();

	}

	public void add_sequence() throws InterruptedException  {
		
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//nz-modal-container/div")));
//		
//		btn_update_sequence = driver.findElement(By.className("ant-input-number-handler-up"));
//		btn_update_sequence.click();
		WebElement btn_valider = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-primary ng-star-inserted']")));

		btn_valider.click();

	}

	public void add_media() {
		WebElement add_media = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//car-display-scenario/div[2]/nz-card[2]/div[2]/div[2]/button")));
		add_media.click();

	}

}
