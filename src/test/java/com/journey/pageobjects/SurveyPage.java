package com.journey.pageobjects;

import java.util.List;

import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class SurveyPage extends BaseDriverClass {

	@FindBy(xpath = "//div/div[2]/div/div[4]")
	@CacheLookup
	private WebElement type_survey;

	@FindBy(xpath = "//car-survey-left-panel/div/div[2]/div/div[1]/div/div")
	@CacheLookup
	private WebElement nps;

	@FindBy(xpath = "//car-survey-builder/div/div/div/div/div[3]")
	@CacheLookup
	private WebElement destinationElement;

	@FindBy(xpath = "//car-survey-left-panel/div/div[2]/div/div[2]/div/div")
	@CacheLookup
	private WebElement csat;

	@FindBy(xpath = "//car-survey-left-panel/div/div[2]/div/div[3]/div/div")
	@CacheLookup
	private WebElement csat_critere;

	@FindBy(xpath = "//car-survey-left-panel/div/div[2]/div/div[4]/div/div")
	@CacheLookup
	private WebElement ces;
	@FindBy(xpath = "//div[2]/span/button")
	private WebElement btn_publish;

	public SurveyPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void create_survey_button() {
		WebElement btn_create_survey =wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[2]/div/div/button")));
		btn_create_survey.click();

	}

	public void choice_type_survey() {
		wait.until(ExpectedConditions.elementToBeClickable(type_survey)).click();

	}

	public void create_survey() {

		// Créer une instance de la classe Actions
		Actions actions = new Actions(driver);

		// Attente jusqu'à ce que les éléments soient visibles

		wait.until(ExpectedConditions.visibilityOf(nps));
		wait.until(ExpectedConditions.visibilityOf(csat));
		wait.until(ExpectedConditions.visibilityOf(csat_critere));
		wait.until(ExpectedConditions.visibilityOf(ces));

		// Effectuer l'opération de drag and drop
		actions.dragAndDrop(nps, destinationElement).perform();
		actions.dragAndDrop(csat, destinationElement).perform();
		actions.dragAndDrop(csat_critere, destinationElement).perform();
		actions.dragAndDrop(ces, destinationElement).perform();

	}

	public void save_publish_survey() {

		wait.until(ExpectedConditions.elementToBeClickable(btn_publish)).click();

	}

	public WebElement scroll_find_element(String projetName) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> titleElements = wait
				.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("h5")));
		WebElement elementToFind = null;
		for (WebElement element : titleElements) {
			if (element.getText().equals(projetName)) {
				elementToFind = element;
				break;
			}
		}

		Asserts.notNull(elementToFind, "Cannot find project with title " + projetName);
		js.executeScript("arguments[0].scrollIntoView(true);", elementToFind);
		WebElement table = elementToFind.findElement(By.xpath("./../../../.."));
		List<WebElement> tableRows = table.findElement(By.className("ant-table-tbody")).findElements(By.tagName("tr"));
		Asserts.notNull(tableRows, "Cannot find table ");
		WebElement row = tableRows.get(0);
		row.click();
		return row;
	}
}
