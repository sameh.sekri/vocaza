package com.journey.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class UserPage extends BaseDriverClass {

	@FindBy(xpath = "//ul/car-home-side-menu/li[5]/span")
	@CacheLookup
	private WebElement btnUsers;

	public UserPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void click_users() {
		wait.until(ExpectedConditions.elementToBeClickable(btnUsers));
		btnUsers.click();

	}

	public void click_create_users() {
		WebElement btnClickUsers = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//button[@class=\"ant-btn ant-btn-primary ng-star-inserted\"]")));
		btnClickUsers.click();

	}
	/*
	 * Remplir le formulaire de la création d'un user Expert de l'Experience client
	 */

	public void fill_info_user(String email) {
		WebElement name = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[1]/nz-form-item/nz-form-control/div/div/input")));
		name.sendKeys("TestAuto");
		WebElement last_name = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[2]/nz-form-item/nz-form-control/div/div/input")));
		last_name.sendKeys("expert");
		WebElement mail = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[3]/nz-form-item/nz-form-control/div/div/input")));
		mail.sendKeys(email);

		WebElement role = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//nz-select-top-control/nz-select-search")));
		role.click();
		WebElement expert_role = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[1]/nz-option-item[2]")));
		expert_role.click();

		WebElement btn_save = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[3]/button")));
		btn_save.click();
	}

	public void find_user(String user) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ant-table-content")));

		List<WebElement> rows = driver.findElement(By.className("ant-table-content")).findElement(By.xpath("//tbody"))
				.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			// Récupérer la troisième colonne
			WebElement email = row.findElements(By.tagName("td")).get(2);

			// Récupérer le texte de la troisième colonne
			String emailText = email.getText();
			if (user.equals(emailText)) {
				System.out.println("L'utilisateur " + emailText + " est créé.");
			}
		}

	}

	public void disconnect() {
		WebElement avatar = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//car-root/nz-layout/nz-sider/div/span")));
		avatar.click();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@nztype='logout']")));
		
		WebElement btnLogout = driver.findElement(By.xpath("//span[@nztype='logout']"))
				.findElement(By.xpath("parent::*"));
		btnLogout.click();

	}
	public void first_connect() {

		WebElement firstConnect = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath("//a[contains(text(),'Première connexion / Mot de passe oublié ?')]")));

		firstConnect.click();
	}

	

}
