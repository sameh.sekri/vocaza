package com.journey.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class AuthPage extends BaseDriverClass {

	@FindBy(xpath = "//*[@id='username']")
	@CacheLookup
	private WebElement username;

	@FindBy(xpath = "//*[@id='password']")
	@CacheLookup
	private WebElement password;

	@FindBy(xpath = "//*[@id='kc-login']")
	@CacheLookup
	private WebElement loginButton;

	public AuthPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void fill_connect_data(String email, String motDePasse) {
		wait.until(ExpectedConditions.visibilityOf(username));
		username.sendKeys(email);
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(motDePasse);
	}

	public Boolean verifyOnLoginPage() {
		wait.until(ExpectedConditions.visibilityOf(loginButton));
		return loginButton.isDisplayed();
	}

	public void connexion() {
		wait.until(ExpectedConditions.elementToBeClickable(loginButton));
		loginButton.click();
	}
}
