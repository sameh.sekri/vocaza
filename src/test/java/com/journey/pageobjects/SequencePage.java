package com.journey.pageobjects;

import java.util.List;

import org.apache.hc.core5.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class SequencePage extends BaseDriverClass {

	@FindBy(xpath = "//input[@type='radio']")
	@CacheLookup
	private WebElement radio_mail;
	private WebElement btn_valider;
	private WebElement name;
	private WebElement prefix;
	private WebElement object_email;
	private WebElement btn_publish;
	private WebElement btn_add_media;
	private WebElement btn_scenario;

	public SequencePage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		PageFactory.initElements(driver, this);
	}

	public void create_sollicitaion() {

		radio_mail = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3/label/span[2]")));
		radio_mail.click();

		btn_valider = driver
				.findElement(By.xpath("//*[@id=\"cdk-overlay-0\"]/nz-modal-container/div/div/div[3]/button"));

		btn_valider.click();

	}

	public void create_email() {

		name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='name']")));
		name.sendKeys("test1");
		prefix = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='email']")));
		prefix.sendKeys("test1.test");
		object_email = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//textarea[@class='ant-input ant-input-stepperless ng-untouched ng-pristine ng-valid']")));
		object_email.sendKeys("Enquête");
		WebElement image_logo = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[1]/div/car-mail-column/div/app-visual-element")));
		image_logo.click();
		WebElement add_image = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-lg']")));
		add_image.click();
		WebElement image_selected = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//car-imagelibrary/div[2]/div[1]/div/div[2]")));
		image_selected.click();
		WebElement btn_add = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-primary ant-btn-sm']")));
		btn_add.click();

	}

	public void publish_mail() {

		btn_publish = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//nz-space/div[2]/button")));

		btn_publish.click();

	}

	public void click_scenario() {

		btn_scenario = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), 'Scénario')]")));
		btn_scenario.click();

	}

	public void add_media_mail() {
		btn_add_media = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='ant-btn ant-btn-primary']")));

		btn_add_media.click();

	}

	public WebElement scroll_find_element(String projetName) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> titleElements = wait
				.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("h5")));
		WebElement elementToFind = null;
		for (WebElement element : titleElements) {
			if (element.getText().equals(projetName)) {
				elementToFind = element;
				break;
			}
		}

		Asserts.notNull(elementToFind, "Cannot find project with title " + projetName);
		js.executeScript("arguments[0].scrollIntoView(true);", elementToFind);
		WebElement table = elementToFind.findElement(By.xpath("./../../../.."));
		List<WebElement> tableRows = table.findElement(By.className("ant-table-tbody")).findElements(By.tagName("tr"));
		Asserts.notNull(tableRows, "Cannot find table ");
		WebElement row = tableRows.get(0);
		row.click();
		return row;
	}
}
