package com.journey.pageobjects;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.BaseDriverClass;

public class ProjectPage extends BaseDriverClass {

    @FindBy(xpath = "//div/div[1]/h3/button")
    @CacheLookup
    private WebElement btn_creer;

    @FindBy(xpath = "//div[1]/input")
    @CacheLookup
    private WebElement name_project;

    @FindBy(xpath = "//nz-select/nz-select-top-control")
    @CacheLookup
    private WebElement listPlan;

    @FindBy(xpath = "//div[1]/nz-option-item[1]")
    @CacheLookup
    private WebElement assurance;

    @FindBy(className = "ant-checkbox")
    @CacheLookup
    private WebElement popup;

    @FindBy(xpath = "//div[3]/button[2]/span")
    @CacheLookup
    private WebElement btn_valider;

    @FindBy(xpath = "//div[3]/button")
    @CacheLookup
    private WebElement btn_fermer;

    @FindBy(xpath = "//div[3]/button")
    @CacheLookup
    private WebElement btn_creer_name_plan;
    //private String projectName ="Selenium";
    public ProjectPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    public void create_project_buton() {
        wait.until(ExpectedConditions.elementToBeClickable(btn_creer)).click();
    }

    public void enter_name_project(String projectName) {
    	//projectName = this.projectName;
        wait.until(ExpectedConditions.visibilityOf(name_project)).sendKeys(projectName);
    }

    public void select_Plan_Analyse() {
        wait.until(ExpectedConditions.elementToBeClickable(listPlan)).click();
        wait.until(ExpectedConditions.elementToBeClickable(assurance)).click();
    }

    public void create_name_plan_popup() {
        wait.until(ExpectedConditions.elementToBeClickable(btn_creer_name_plan)).click();
    }
    
    public void waitForElementToDisappear(By locator) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }
    
    public void interaction_list() throws InterruptedException {

	WebElement popup = new WebDriverWait(driver, Duration.ofSeconds(30))
			.until(ExpectedConditions.elementToBeClickable(By.className("ant-checkbox")));

		// Récupérer toutes les cases à cocher de la checklist
		List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
		// Parcourir chaque case à cocher et la cocher
		checkboxes.forEach((chk) -> {
			chk.click();
		});

		WebElement btn_valider = new WebDriverWait(driver, Duration.ofSeconds(20))
				.until(ExpectedConditions.elementToBeClickable(
						By.xpath("//*[@id=\"cdk-overlay-2\"]/nz-modal-container/div/div/div[3]/button[2]")));

		btn_valider.click();
		Thread.sleep(5000);
    }

    public void fermer_popup() throws InterruptedException {
    	Thread.sleep(5000);
        wait.until(ExpectedConditions.elementToBeClickable(btn_fermer)).click();
    }

    public WebElement scroll_find_element(String projetName) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement elementToFind = null;
        int attempts = 0;
      
        while (attempts < 3) {
            try {
                
                List<WebElement> titleElements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("h5")));
                for (WebElement element : titleElements) {
                    if (element.getText().equals(projetName)) {
                        elementToFind = element;
                        break;
                    }
                }

                if (elementToFind == null) {
                 
                    throw new NoSuchElementException("Cannot find project with title " + projetName);
                }

                js.executeScript("arguments[0].scrollIntoView(true);", elementToFind);
                
                WebElement table = elementToFind.findElement(By.xpath("./../../../.."));
                List<WebElement> tableRows = table.findElement(By.className("ant-table-tbody")).findElements(By.tagName("tr"));
                
                if (tableRows == null || tableRows.isEmpty()) {
                
                    throw new NoSuchElementException("Cannot find table rows for project with title " + projetName);
                }
                
                WebElement row = tableRows.get(0);
                return row;
            } catch (StaleElementReferenceException e) {
                
                attempts++;
            } catch (NoSuchElementException e) {
                attempts++;
            } catch (Exception e) {
               
                throw new RuntimeException("Failed to locate element: " + e.getMessage());
            }
        }
        throw new RuntimeException("Failed to locate project with title " + projetName + " after multiple attempts");
    }
}
