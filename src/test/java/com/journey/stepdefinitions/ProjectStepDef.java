package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.ProjectPage;

import io.cucumber.java.en.When;

public class ProjectStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private ProjectPage projectPage;
	
	public ProjectStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		projectPage = new ProjectPage(driver, wait);
	}
	@When("l'utilisateur clique sur le bouton créer un projet")
	public void l_utilisateur_clique_sur_le_bouton_créer_un_projet() {
		projectPage.create_project_buton();
	}

	@When("l'utilisateur crée un projet nommé {string}")
	public void l_utilisateur_crée_un_projet_nommé(String projectName) {
		projectPage.enter_name_project(projectName);
	}
	
	@When("l'utilisateur clique sur le plan d'analyse {string}")
	public void l_utilisateur_clique_sur_le_plan_d_analyse(String string) {
		projectPage.select_Plan_Analyse();
	}
	
	
	@When("l'utilisateur clique sur le bouton Créer")
	public void l_utilisateur_clique_sur_le_bouton_créer() {
		projectPage.create_name_plan_popup();
	}
	@When("l'utilisateur coche toutes les interactions")
	public void l_utilisateur_coche_toutes_les_interactions() throws InterruptedException {
		projectPage.interaction_list();
	}

	@When("l'utilisateur clique sur le bouton de fermeture")
	public void l_utilisateur_clique_sur_le_bouton_de_fermeture() throws InterruptedException {
		projectPage.fermer_popup();
	}
//
//	@When("l'utilisateur navigue vers le projet créé")
//	public void l_utilisateur_navigue_vers_le_projet_créé() {
//		projectPage.scroll_find_element("Selenium");
//	}


}
