package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.UserPage;

import io.cucumber.java.en.When;

public class UserStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private UserPage userPage;
	
	
	public UserStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
	driver = Hooks.driver;
	wait = new WebDriverWait(driver, Duration.ofSeconds(20));
	userPage = new UserPage(driver, wait);
	}
	
	@When("l'utilisateur clique sur utilisateurs")
	public void l_utilisateur_clique_sur_utilisateurs() {
	   userPage.click_users();
	}

	@When("l'utilisateur clique sur Créer un utilisateur")
	public void l_utilisateur_clique_sur_créer_un_utilisateur() {
	    userPage.click_create_users();
	    
	}

	@When("l'utilisateur Remplit tous les champs avec des valeurs correctes de l'utilisateur  {string}, choisir le role Expert de l'éxpèrience client")
	public void l_utilisateur_remplit_tous_les_champs_avec_des_valeurs_correctes_de_l_utilisateur_choisir_le_role_expert_de_l_éxpèrience_client(String email) { 
	    userPage.fill_info_user(email);
	}

	@When("l'utilisateur Vérifie que {string} est bien ajouté et affiché dans la liste des utilisateurs")
	public void l_utilisateur_vérifie_que_est_bien_ajouté_et_affiché_dans_le_liste_des_utilisateurs(String user) {
		userPage.find_user(user);
	}

	@When("l'utlisateur se déconnecte du compte actuel")
	public void l_utlisateur_se_déconnecte_du_compte_actuel() {
	    userPage.disconnect();
	}

}
