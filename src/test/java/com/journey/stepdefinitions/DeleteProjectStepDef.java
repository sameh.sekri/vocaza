package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.DeleteProjectPage;

import io.cucumber.java.en.When;

public class DeleteProjectStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private DeleteProjectPage deleteProjectPage;
	private WebElement projectToDelete;
	
	public DeleteProjectStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		deleteProjectPage = new DeleteProjectPage(driver, wait);
	}
	
	@When("l'utilisateur scrolle vers le projet {string}")
	public void l_utilisateur_scrolle_vers_le_projet(String projectName) {
		projectToDelete = deleteProjectPage.scroll_find_element(projectName);
	}

	@When("l'utilisateur clique sur les trois points et choisit l'option Supprimer le projet")
	public void l_utilisateur_clique_sur_les_trois_points_et_choisit_l_option_supprimer_le_projet() throws InterruptedException {
	   deleteProjectPage.clickDeleteMenu(projectToDelete);
	}

	@When("l'utlisateur confirme la suppresion en cliquant sur le bouton Supprimer")
	public void l_utlisateur_confirme_la_suppresion_en_cliquant_sur_le_bouton_supprimer() {
		 deleteProjectPage.confirm_delete();
	}
}
