package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.ConnectUserPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ConnectUserStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private ConnectUserPage connectUserPage;
	
	public ConnectUserStepDef() {
		if (Hooks.driver == null) {
			PageFactory.initElements(driver, this);
		}
	driver = Hooks.driver;
	wait = new WebDriverWait(driver, Duration.ofSeconds(20));
	connectUserPage = new ConnectUserPage(driver, wait);
	}
	
	@When("l'utilisateur clique Première connexion \\/ Mot de passe oublié ?")
	public void l_utilisateur_clique_première_connexion_mot_de_passe_oublié() {
	    connectUserPage.first_connect();
	}

	@When("l'utilisateur entre son  email {string} et clique sur envoyer")
	public void l_utilisateur_entre_son_email_et_clique_sur_envoyer(String email) {
	   connectUserPage.enter_email(email);
	}


	@When("l'utilisateur verifie son email {string} en cliquant sur Definir mon nouveau mot de passe")
	public void l_utilisateur_verifie_son_email_en_cliquant_sur_definir_mon_nouveau_mot_de_passe(String email) throws Exception {
	   connectUserPage.verify_email(email);
	}

	@When("l'utilisateur entre son nouveau mot de passe {string} et clique sur envoyer")
	public void l_utilisateur_entre_son_nouveau_mot_de_passe_et_clique_sur_envoyer(String newPassword) {
		connectUserPage.change_password(newPassword);
	}
	@Then("l'utilisateur du compte {string} et mot de passe {string} se connecte dans la page Mes projets")
	public void l_utilisateur_du_compte_et_mot_de_passe_se_connecte_dans_la_page_mes_projets(String username, String password) {
	    connectUserPage.verify_project_page(username,password);
	}


}
