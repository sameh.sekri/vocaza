package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.RelancePage;


import io.cucumber.java.en.When;

public class RelanceStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private RelancePage relancePage;
	
	
	public RelanceStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
	driver = Hooks.driver;
	wait = new WebDriverWait(driver, Duration.ofSeconds(30));
	relancePage = new RelancePage(driver, wait);
	}
	@When("l'utilisateur clique sur le bouton ajouter une relance")
	public void l_utilisateur_clique_sur_le_bouton_ajouter_une_relance() {
	    relancePage.add_relance();
	}

	@When("l'utilisateur configure la séquence de la relance")
	public void l_utilisateur_configure_la_séquence_de_la_relance() throws InterruptedException {
	   relancePage.add_sequence();
	}

	@When("l'utilisateur ajoute un media email pour la relance")
	public void l_utilisateur_ajoute_un_media_email_pour_la_relance() {
	    relancePage.add_media();
	}

	@When("L'utilisateur choisit un media de relance et valide")
	public void l_utilisateur_choisit_un_media_de_relance_et_valide() {
	    relancePage.add_email_relance();
	}

	@When("l'utilisateur clique publier et quitter la relance")
	public void l_utilisateur_clique_publier_et_quitter_la_relance() {
	    relancePage.publish_relance();
	}
}
