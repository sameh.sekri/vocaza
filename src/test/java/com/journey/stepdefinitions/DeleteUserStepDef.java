package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.DeleteUserPage;

import io.cucumber.java.en.When;

public class DeleteUserStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private DeleteUserPage deleteUserPage;
	private WebElement user;

	public DeleteUserStepDef() {
		if (Hooks.driver == null) {
			PageFactory.initElements(driver, this);
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		deleteUserPage = new DeleteUserPage(driver, wait);
	}

	@When("l'utilisateur cherche le compte {string} à supprimer")
	public void l_utilisateur_cherche_le_compte_à_supprimer(String username) {
		user = deleteUserPage.find_user(username);
	}

	@When("l'utilisateur clique sur le bouton supprimer")
	public void l_utilisateur_clique_sur_le_bouton_supprimer() {
		deleteUserPage.click_delete(user);
	}

	@When("l'utilisateur confirme la suppression du compte utlisateur {string}")
	public void l_utilisateur_confirme_la_suppression_du_compte_utlisateur(String username) {
		deleteUserPage.confirm_delete(username);
	}

}
