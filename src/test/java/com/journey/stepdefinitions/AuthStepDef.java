package com.journey.stepdefinitions;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.AuthPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class AuthStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private AuthPage authPage;

	
	public AuthStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		authPage = new AuthPage(driver, wait);
	}
	
	@Given("L'utilisateur navigue vers la page authentification Journey")
	public void l_utilisateur_navigue_vers_la_page_authentification_journey() {
		 Assert.assertTrue("La page d'authentification n'est pas affichée", authPage.verifyOnLoginPage());
	}
	
	@When("l'utilisateur saisit son email {string} et son mot de passe {string}")
	public void l_utilisateur_saisit_son_email_et_son_mot_de_passe(String email, String motDePasse) {
		authPage.fill_connect_data(email, motDePasse);
	}




@When("l'utilisateur clique sur le bouton de connexion")
public void l_utilisateur_clique_sur_le_bouton_de_connexion() {
	authPage.connexion();
}

}
