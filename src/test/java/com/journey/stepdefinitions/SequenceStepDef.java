package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.SequencePage;

import io.cucumber.java.en.When;

public class SequenceStepDef {
	private WebDriver driver;
	private WebDriverWait wait;
	private SequencePage sequencePage;

	public SequenceStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		sequencePage = new SequencePage(driver, wait);
	}

	@When("l'utilisateur cherche le projet {string}")
	public void l_utilisateur_cherche_le_projet(String projectName) {
		sequencePage.scroll_find_element(projectName);
	}

	@When("l'utilisateur clique sur l'onglet Scénario")
	public void l_utilisateur_clique_sur_l_onglet_scénario() {
		sequencePage.click_scenario();
	}

	@When("l'utilisateur ajoute un media email")
	public void l_utilisateur_ajoute_un_media_email() {
		sequencePage.add_media_mail();
	}

	@When("L'utilisateur choisit le media email pour la premiere solicitation et valide")
	public void l_utilisateur_choisit_le_media_email_pour_la_premiere_solicitation_et_valide()
			throws InterruptedException {
		sequencePage.create_sollicitaion();
	}

	@When("l'utilisateur remplit les paramètres d'envoi")
	public void l_utilisateur_remplit_les_paramètres_d_envoi() {
		sequencePage.create_email();
	}

	@When("l'utilisateur clique sur publier et quitter Première sollicitation")
	public void l_utilisateur_clique_sur_publier_et_quitter_première_sollicitation() {
		sequencePage.publish_mail();
	}

}
