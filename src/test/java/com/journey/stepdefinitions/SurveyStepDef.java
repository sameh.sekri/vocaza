package com.journey.stepdefinitions;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.journey.common.Hooks;
import com.journey.pageobjects.SurveyPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class SurveyStepDef {

	private WebDriver driver;
	private WebDriverWait wait;
	private SurveyPage surveyPage;

	public SurveyStepDef() {
		if (Hooks.driver == null) {
			// Gérer le cas où le driver est null
			throw new IllegalStateException("Le WebDriver n'est pas initialisé dans la classe Hooks");
		}
		driver = Hooks.driver;
		wait = new WebDriverWait(driver, Duration.ofSeconds(20));
		surveyPage = new SurveyPage(driver, wait);
	}

	@When("l'utilisateur selectionne une interaction dans le projet {string}")
	public void l_utilisateur_selectionne_une_interaction_dans_le_projet(String projectName) {
		surveyPage.scroll_find_element(projectName);
	}

	@When("l'utilisateur clique sur créer")
	public void l_utilisateur_clique_sur_créer() {
		surveyPage.create_survey_button();
	}

	@When("l'utilisateur choisit une enquête en partant d'un modèle vierge")
	public void l_utilisateur_choisit_une_enquête_en_partant_d_un_modèle_vierge() {
		surveyPage.choice_type_survey();
	}

	@When("l'utilisateur crée une enquête")
	public void l_utilisateur_crée_une_enquête() {
		surveyPage.create_survey();
	}

	@Given("l'utilisateur clique sur publier et quitter pour enregistrer")
	public void l_utilisateur_clique_sur_publier_et_quitter_pour_enregistrer() {
		surveyPage.save_publish_survey();
	}

}
