package com.journey.testng.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/resources/features",
    glue = {"com.journey.stepdefinitions", "com.journey.common"},
    plugin = { "pretty", "html:target/cucumber-reports.html" }
)
public class Test_Runner {
}
