package com.journey.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListLabelsResponse;

public class GmailOAuth2 {

	private static final String APPLICATION_NAME = "Vocaza Journey";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	public static void main(String[] args) throws IOException, GeneralSecurityException {
		// Load client secrets.
		InputStream in = GmailOAuth2.class.getResourceAsStream("/credentials.json");
		if (in == null) {
			System.out.println("Resource not found: /credentials.json");
			return;
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				GoogleNetHttpTransport.newTrustedTransport(), JSON_FACTORY, clientSecrets,
				Collections.singleton(GmailScopes.GMAIL_READONLY))
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline").build();

		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8080).setCallbackPath("/Callback")
				.build();
		Credential credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");

		// Print access token and refresh token
		System.out.println("Access Token: " + credential.getAccessToken());
		System.out.println("Refresh Token: " + credential.getRefreshToken());

		// Use the access token to authenticate with the Gmail API
		Gmail service = new Gmail.Builder(GoogleNetHttpTransport.newTrustedTransport(), JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME).build();

		// Example: Print the labels in the user's account.
		String user = "test.vocaza@gmail.com";
		ListLabelsResponse listResponse = service.users().labels().list(user).execute();
		List<Label> labels = listResponse.getLabels();
		System.out.println("Labels:");
		for (Label label : labels) {
			System.out.printf("- %s\n", label.getName());
		}
	}
}
