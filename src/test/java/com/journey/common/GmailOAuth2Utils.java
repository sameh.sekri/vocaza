package com.journey.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Folder;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;

public class GmailOAuth2Utils {
	private Folder folder;
	private static final String CREDENTIALS_FILE_PATH = "credentials.json";

	public Gmail connectUser() throws IOException, GeneralSecurityException {
		/*************************************************************************************************
		 * 
		 * Uncomment this bloc if we change another user account we need to confirm
		 * consent
		 */
//			InputStream in = getClass().getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);
		// Delete old tokens to force new authentication.
//	        File tokensDirectory = new File("tokens");
//	        if (tokensDirectory.exists()) {
//	            for (File file : tokensDirectory.listFiles()) {
//	                if (!file.isDirectory()) {
//	                    file.delete();
//	                }
//	            }
//	            tokensDirectory.delete();
//	        }
		/****************************************************************************************************/

		// Build a new authorized API client service.
		NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Credential credential;

		credential = getCredentials(HTTP_TRANSPORT);

		Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JacksonFactory.getDefaultInstance(), credential)
				.setApplicationName("Vocaza	Journey").build();

		return service;
	}

	private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = getClass().getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new IOException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JacksonFactory.getDefaultInstance(),
				new InputStreamReader(in));
		List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
				JacksonFactory.getDefaultInstance(), clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new File("tokens"))).setAccessType("offline").build();

		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8080).setCallbackPath("/Callback")
				.build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	public List<Message> getMessages(Gmail service, String username, String query) throws IOException {
		// Use the specific email address for the user

		ListMessagesResponse listResponse = service.users().messages().list(username).setQ(query).execute();
		List<Message> messages = listResponse.getMessages();
		List<Message> fullMessages = null;

		if (messages == null || messages.isEmpty()) {
			System.out.println("No unread messages found from the specified sender with the specified subject.");
		} else {
			System.out.println("Unread messages from the specified sender with the specified subject:");
			fullMessages = new ArrayList<Message>();
			int limit = 5; // Limit to 10 messages

			if (messages.size() > 1) {
				// Sort messages by internal date in descending order
				Collections.sort(messages, new Comparator<Message>() {
					@Override
					public int compare(Message m1, Message m2) {
						// Handle null internalDate gracefully
						if (m1.getInternalDate() == null && m2.getInternalDate() == null) {
							return 0;
						} else if (m1.getInternalDate() == null) {
							return 1; // m1 is considered greater if its internalDate is null
						} else if (m2.getInternalDate() == null) {
							return -1; // m2 is considered greater if its internalDate is null
						} else {
							return m2.getInternalDate().compareTo(m1.getInternalDate());
						}
					}
				});
			}
			for (int i = 0; i < Math.min(messages.size(), limit); i++) {
				Message message = messages.get(i);

				Message fullMessage = service.users().messages().get(username, message.getId()).execute();
				fullMessages.add(fullMessage);
			}
		}

		System.out.println("Emails read");
		return fullMessages;
	}

	public String getEmailContent(Message message) {
		StringBuilder emailContent = new StringBuilder();
		if (message.getPayload().getParts() != null) {
			for (MessagePart part : message.getPayload().getParts()) {
				if (part.getBody() != null && part.getBody().getData() != null) {
					emailContent.append(new String(part.getBody().decodeData()));
				}
			}
		} else if (message.getPayload().getBody() != null && message.getPayload().getBody().getData() != null) {
			emailContent.append(new String(message.getPayload().getBody().decodeData()));
		}
		return emailContent.toString();
	}

	public List<String> extractUrls(String text) {
		List<String> urls = new ArrayList<>();
		String regex = "(http|https|ftp|ftps)://[^\\s/$.?#].[^\\s]*";
		Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(text);
		while (m.find()) {
			urls.add(text.substring(m.start(0), m.end(0)));
		}
		return urls;
	}

}
