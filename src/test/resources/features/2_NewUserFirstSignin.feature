
@tag

Feature: Première connexion d'un nouvel utilisateur

@tag1

    Scenario: connnexion d'un utlisateur et changement de son nouveau mot de passe
    
    When l'utilisateur clique Première connexion / Mot de passe oublié ?
    And l'utilisateur entre son  email "<email>" et clique sur envoyer
    And l'utilisateur verifie son email "<email>" en cliquant sur Definir mon nouveau mot de passe
    And l'utilisateur entre son nouveau mot de passe "<new_password>" et clique sur envoyer
    Then l'utilisateur du compte "<email>" et mot de passe "<new_password>" se connecte dans la page Mes projets
    And  l'utlisateur se déconnecte du compte actuel

   Examples:
     
    | email	| new_password	|
    | test.vocaza@gmail.com|	Voc@zaTest.20241	|
    
  