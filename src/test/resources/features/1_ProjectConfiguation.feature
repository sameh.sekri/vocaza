@tag

Feature: Smoke test standard
Background:
   
   Given L'utilisateur navigue vers la page authentification Journey
   When  l'utilisateur saisit son email "s.sekri@vocaza.com" et son mot de passe "S@meh.sekri-2024"
   And   l'utilisateur clique sur le bouton de connexion
 
 
  @tag1
  
 Scenario: Créer un nouveau projet 

    When l'utilisateur clique sur le bouton créer un projet
    And l'utilisateur crée un projet nommé "<nom_du_projet>"
    And l'utilisateur clique sur le plan d'analyse "<plan>"
    And l'utilisateur clique sur le bouton Créer
    And l'utilisateur coche toutes les interactions
    And l'utilisateur clique sur le bouton de fermeture
    

  Examples:
    | nom_du_projet | plan     	|
    | Selenium      | Assurance	|
    
  @tag2
   

  Scenario:  Créer et publier une enquête dans Journey
    
    When l'utilisateur selectionne une interaction dans le projet "<nom_du_projet>"
    And  l'utilisateur clique sur créer
    And  l'utilisateur choisit une enquête en partant d'un modèle vierge
    And  l'utilisateur crée une enquête
    And  l'utilisateur clique sur publier et quitter pour enregistrer
    
    Examples:
    | nom_du_projet | 
    | Selenium      | 
    
    
     @tag3
     
   Scenario:  Créer une Séquence - Première sollicitation

    When l'utilisateur cherche le projet "<nom_du_projet>"
    And l'utilisateur clique sur l'onglet Scénario
    And l'utilisateur ajoute un media email
    And L'utilisateur choisit le media email pour la premiere solicitation et valide
    And l'utilisateur remplit les paramètres d'envoi
    And l'utilisateur clique sur publier et quitter Première sollicitation
    
     Examples:
    | nom_du_projet | 
    | Selenium      | 
    
    
     @tag4
   
  Scenario:  Créer une relance
  
 		When l'utilisateur cherche le projet "<nom_du_projet>"
 		And l'utilisateur clique sur l'onglet Scénario
    And l'utilisateur clique sur le bouton ajouter une relance 
    And l'utilisateur configure la séquence de la relance 
 		And l'utilisateur ajoute un media email pour la relance
 		And L'utilisateur choisit un media de relance et valide
 		And l'utilisateur remplit les paramètres d'envoi
 		And l'utilisateur clique publier et quitter la relance
 		
    Examples:
    | nom_du_projet | 
    | Selenium      | 
    
    
    @tag5
    
   Scenario:  Création d'un utilisateur
     
    When l'utilisateur clique sur utilisateurs
    And l'utilisateur clique sur Créer un utilisateur
    And l'utilisateur Remplit tous les champs avec des valeurs correctes de l'utilisateur  "<utilisateur>", choisir le role Expert de l'éxpèrience client
    And l'utilisateur Vérifie que "<utilisateur>" est bien ajouté et affiché dans la liste des utilisateurs
    And l'utlisateur se déconnecte du compte actuel
   
    
     Examples:
    | utilisateur | 
    | test.vocaza@gmail.com|
    
   
    
    @tag7
   
  Scenario:  Créer un import manuel
  
  And je clique sur l'onglet Diffusion
  And je clique sur import manuel
  And je clique sur le bouton continuer
  And je clique sur Selectionez et glissez votre fichier ici
  And je choisis le fichier à importer et je clique sur ouvrir
  And je coche le RGPD
  And je clique sur le bouton suivant
  And je clique sur le bouton suivant pour finaliser la correspondance
  And je coche la regle de diffusion 'Toujours importer'
  And je clique sur le bouton Suivant pour acceder au recapitulatif
  And je clique sur le bouton Terminer 
  
  
     @tag8
   
  Scenario:  Supprimer le projet crée
  
 		When l'utilisateur scrolle vers le projet "<nom_du_projet>"
 		And  l'utilisateur clique sur les trois points et choisit l'option Supprimer le projet
 		And  l'utlisateur confirme la suppresion en cliquant sur le bouton Supprimer
 
 
    Examples:
    | nom_du_projet | 
    | Selenium      | 
    
