# Guide d'Upload de Projet sur GitLab

Pour uploader votre projet sur GitLab, suivez ces étapes :

1. **Créer un nouveau repository sur GitLab :**
   - Connectez-vous à votre compte GitLab.
   - Cliquez sur le bouton pour créer un nouveau projet/repository.

2. **Initialiser un repository Git local :**
   - Assurez-vous d'avoir Git installé sur votre machine.
   - Ouvrez un terminal ou une ligne de commande.
   - Naviguez vers le répertoire racine de votre projet local.

3. **Initialiser Git dans votre projet local :**
   - Utilisez la commande `git init` pour initialiser Git dans votre répertoire local si ce n'est pas déjà fait.

4. **Ajouter vos fichiers au suivi de Git :**
   - Utilisez `git add .` pour ajouter tous les fichiers de votre répertoire local au suivi de Git. Si vous souhaitez ajouter des fichiers spécifiques, remplacez `.` par les noms des fichiers.

5. **Faire un commit des changements :**
   - Utilisez `git commit -m "Premier commit"` pour créer un nouveau commit avec vos changements locaux.

6. **Ajouter l'URL de votre repository GitLab :**
   - Utilisez `git remote add origin <URL_de_votre_repository_GitLab>` pour lier votre repository local à celui sur GitLab. Remplacez `<URL_de_votre_repository_GitLab>` par l'URL que vous obtenez depuis GitLab.

7. **Pousser votre code vers GitLab :**
   - Utilisez `git push -u origin master` pour pousser votre code local vers le repository GitLab. Cela enverra vos commits vers GitLab et les rendra disponibles dans votre repository en ligne.
