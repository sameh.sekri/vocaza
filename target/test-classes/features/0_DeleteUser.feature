@tag

Feature: Supprimer un utilisateur

   Background:
    
   Given L'utilisateur navigue vers la page authentification Journey
   When  l'utilisateur saisit son email "s.sekri@vocaza.com" et son mot de passe "S@meh.sekri-2024"
   And   l'utilisateur clique sur le bouton de connexion
 
  @tag1
    
     Scenario:  Supprimer un utilisateur
     
     When l'utilisateur clique sur utilisateurs
     And l'utilisateur cherche le compte "<utilisateur>" à supprimer
     And l'utilisateur clique sur le bouton supprimer
     And l'utilisateur confirme la suppression du compte utlisateur "<utilisateur>"
    
     Examples:
    | utilisateur | 
    | test.vocaza@gmail.com|