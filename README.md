# Projet d'Automatisation Vocaza Journey

## Introduction

Ce projet est développé dans le but d'automatiser les flux de test de l'application web Vocaza Journey. Le projet est construit en utilisant Java 17, Selenium WebDriver, Cucumber et respecte le patron de conception Page Object Model (POM). Maven est utilisé pour la gestion du projet, avec JUnit, TestNG et Cucumber-reports pour la génération des résultats de test, et le Chrome WebDriver pour l'automatisation du navigateur.

## Architecture du Projet

Le projet est organisé dans les packages suivants :

- **com.journey.common** : Contient les classes communes telles que `BaseDriver`, dont héritent toutes les classes "Page", et `Hooks`, qui inclut la configuration, l'initialisation et la fermeture du Chrome WebDriver.
- **com.journey.pageobjects** : Contient les classes de pages suivant le patron de conception POM.
- **com.journey.stepdefinitions** : Contient les classes de définitions des étapes.
- **com.journey.testng.runner** : Contient la classe `TestRunner.java` pour l'exécution des tests.
- **com.journey.utils** : Contient les classes utilitaires avec des fonctions d'aide.

## Structure du Projet

```
src
└── test
    └── java
        └──com
        │   └──journey
        │       ├── common
        │       │   ├── BaseDriver.java
        │       │   └── Hooks.java
        │       ├── pageobjects
        │       │   ├── LoginPage.java
        │       │   ├── HomePage.java
        │       │   └── ...
        │       ├── stepdefinitions
        │       │   ├── LoginSteps.java
        │       │   ├── HomeSteps.java
        │       │   └── ...
        │       ├── testng
        │       │   └── runner
        │       │       └── TestRunner.java
        │       └── utils
        │           ├── ...
        │           └── ...
        └── resources
            └── features
            │   ├── 1_ProjectConfiguation.feature
            │   └── ...
            └── application.properties
```

## Installation

1. **Cloner le dépôt :**
   ```sh
   git clone https://gitlab.com/your-username/your-repo-name.git
   cd your-repo-name
   
2. **Installer les dépendances :**
   Assurez-vous d'avoir Maven installé, puis exécutez :
   ```sh
   mvn clean install

3. **Configurer WebDriver :**
   Assurez-vous d'avoir ChromeDriver installé et ajouté à votre PATH système. Vous pouvez télécharger ChromeDriver [ici](https://sites.google.com/a/chromium.org/chromedriver/downloads).

## Exécution des Tests

### Utilisation de Maven

Pour exécuter les tests avec Maven, utilisez la commande suivante :
```sh
mvn test

### Utilisation de TestNG

Pour exécuter les tests avec TestNG, utilisez la commande suivante :
```sh
mvn test -DsuiteXmlFile=testng.xml

## Génération des Rapports :

Les résultats des tests seront générés dans le fichier `target/cucumber-reports.html`.

## Déploiement
Le processus de déploiement implique l'utilisation de Docker et la mise en place d'un pipeline GitLab.

### Docker
En cours

### GitLab CI/CD
En cours


